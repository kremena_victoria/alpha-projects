public class PlatiniumPlan extends  HealthInsurancePlan {

    private final double PLATINIUM_PLAN = 0.9;

    @Override
    public double getCoverage() {
        return PLATINIUM_PLAN;
    }

}


