public class SilverPlan extends HealthInsurancePlan {

    private final double SILVER_PLAN = 0.7;

    @Override
    public double getCoverage() {
        return SILVER_PLAN;
    }
}
