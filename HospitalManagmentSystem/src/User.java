public class User {
    private long id;
    private String firstName;
    private String lastName;
    private String gender;
    private String email;

    private boolean insured;
    private HealthInsurancePlan insurancePlan;

    public User(){
        this(0,"","","","",false);
    }

    public User(long id, String firstName, String lastName, String gender, String email, boolean insured) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.email = email;
        this.insured = insured;
    }

    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    protected void setId(long id) {
        this.id = id;
    }

    protected void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    protected void setLastName(String lastName) {
        this.lastName = lastName;
    }

    protected void setGender(String gender) {
        this.gender = gender;
    }

    protected void setEmail(String email) {
        this.email = email;
    }

    protected void setInsured(boolean insured) {
        this.insured = insured;
    }

    public boolean isInsured() {
        return insured;
    }

    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        if(isInsured() == false){
            this.insurancePlan = null;
        }else {
            this.insurancePlan = insurancePlan;
        }
    }

    public HealthInsurancePlan getInsurancePlan() {
        return insurancePlan;
    }

}
