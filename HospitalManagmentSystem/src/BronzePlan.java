public class BronzePlan extends HealthInsurancePlan {

    private final double BRONZE_PLAN = 0.6;

    @Override
    public double getCoverage() {
        return BRONZE_PLAN;
    }
}
