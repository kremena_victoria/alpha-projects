public class Billing {

     public static double computePaymentMethod(Patient patient, double ammount) {
        double coveredAmmount;
        double patientMustPay;

        if (!patient.isInsured()) {
            coveredAmmount = 20;
            patientMustPay = ammount - coveredAmmount;
        } else {
            coveredAmmount = ammount * (patient.getInsurancePlan().getCoverage());
            patientMustPay = ammount - coveredAmmount;
        }

        if (patient.getInsurancePlan() != null) {
            if (patient.getInsurancePlan().getCoverage() == 0.9)
                patientMustPay -= 50;
            else if (patient.getInsurancePlan().getCoverage() == 0.8)
                patientMustPay -= 40;
            else if (patient.getInsurancePlan().getCoverage() == 0.7)
                patientMustPay -= 30;
            else if (patient.getInsurancePlan().getCoverage() == 0.6)
                patientMustPay -= 25;
        }
        if(patientMustPay < 0){
            patientMustPay = 0.0;
        }

            return patientMustPay;
    }
}
