public class Main {
    public static void main(String[] args){

        Patient patient1 = new Patient(43234,"Velko","Milanov","male","velko@yahoo.com", true);
        patient1.setInsurancePlan(new PlatiniumPlan());  // to be not negative
        //System.out.println(patient1.getInsurancePlan().getCoverage());
        System.out.println("Patient1 must do selfpayment with size: " + Billing.computePaymentMethod(patient1, 100)+ "$.");

        Patient patient2 = new Patient(21124, "Petko", "Petkov", "male", "petko@gmail.com", false);
        System.out.println("Patient2 must do selfpayment with size: " + Billing.computePaymentMethod(patient2, 1000) + "$.");

        Patient patient3 = new Patient(23432, "Sisi", "Bonanza", "f", "sisi@aol.de", true);
        patient3.setInsurancePlan(new SilverPlan());
        System.out.println("Patient3 must do selfpayment with size: " + Billing.computePaymentMethod(patient3, 1000) + "$.");

        Doctor d1 = new Doctor(234, "Ivan","Kolarov", "male","ivan@yahoo.com", 4, "des", 2000, true,"Ortopedic");
        //d1.setInsurancePlan(new SilverPlan());


    }
}
