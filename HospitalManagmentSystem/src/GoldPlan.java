public class GoldPlan extends HealthInsurancePlan {

    private final double GOLD_PLAN = 0.8;

    @Override
    public double getCoverage() {
        return GOLD_PLAN;
    }
}
