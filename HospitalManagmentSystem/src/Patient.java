public class Patient extends User{

    private long patientId;


    public Patient(long id, String firstName, String lastName, String gender, String email, boolean insured) {
        super(id, firstName, lastName, gender, email, insured);
        this.patientId = super.getId();
    }

    public long getPatientId() {
        return patientId;
    }

    protected void setPatientId(long patientId) {
        this.patientId = super.getId();
    }


}
