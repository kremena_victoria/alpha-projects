public class Doctor extends Staff{
    private long doctorId;
    private String specialization;


    public Doctor(long id, String firstName, String lastName, String gender, String email, int yearsOfExpiriance, String description, double salary, boolean insured, String specialization) {
        super(id, firstName, lastName, gender, email, yearsOfExpiriance, description, salary, insured);
        this.doctorId = super.getId();
        this.specialization = specialization;
    }

    public long getDoctorId() {
        return doctorId;
    }

    public String getSpecialization() {
        return specialization;
    }

    protected void setDoctorId(long doctorId) {
        this.doctorId = super.getId();
    }

    protected void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    @Override
    protected void setInsured(boolean insured) {
        super.setInsured(insured);
    }

    @Override
    public boolean isInsured() {
        return super.isInsured();
    }

    @Override
    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        super.setInsurancePlan(insurancePlan);
    }

    @Override
    public HealthInsurancePlan getInsurancePlan() {
        return super.getInsurancePlan();
    }
}
