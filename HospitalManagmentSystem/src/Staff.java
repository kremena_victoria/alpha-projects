public class Staff extends User {

    private long staffId;
    private int yearsOfExpiriance;
    private String description;
    private double salary;


    public Staff(long id, String firstName, String lastName, String gender, String email, int yearsOfExpiriance, String description, double salary, boolean insured) {
        super(id, firstName, lastName, gender, email, insured);
        this.staffId = super.getId();
        this.yearsOfExpiriance = yearsOfExpiriance;
        this.description = description;
        this.salary = salary;
    }

    public long getStaffId() {
        return super.getId();
    }

    public int getYearsOfExpiriance() {
        return yearsOfExpiriance;
    }

    public String getDescription() {
        return description;
    }

    public double getSalary() {
        return salary;
    }

    protected void setYearsOfExpiriance(int yearsOfExpiriance) {
        this.yearsOfExpiriance = yearsOfExpiriance;
    }

    protected void setDescription(String description) {
        this.description = description;
    }

    protected void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    protected void setInsured(boolean insured) {
        super.setInsured(insured);
    }

    @Override
    public boolean isInsured() {
        return super.isInsured();
    }

    @Override
    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        super.setInsurancePlan(insurancePlan);
    }

    @Override
    public HealthInsurancePlan getInsurancePlan() {
        return super.getInsurancePlan();
    }
}
