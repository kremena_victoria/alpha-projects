public class Nurse extends Staff {

    private long nurseId;

    public Nurse(long id, String firstName, String lastName, String gender, String email, int yearsOfExpiriance, String description, double salary, boolean insured) {
        super(id, firstName, lastName, gender, email, yearsOfExpiriance, description, salary, insured);
        this.nurseId = super.getId();
    }

    public long getNurseId() {
        return nurseId;
    }

    protected void setNurseId(long nurseId) {
        this.nurseId = super.getId();
    }

    @Override
    protected void setInsured(boolean insured) {
        super.setInsured(insured);
    }

    @Override
    public boolean isInsured() {
        return super.isInsured();
    }

    @Override
    public void setInsurancePlan(HealthInsurancePlan insurancePlan) {
        super.setInsurancePlan(insurancePlan);
    }

    @Override
    public HealthInsurancePlan getInsurancePlan() {
        return super.getInsurancePlan();
    }
}
