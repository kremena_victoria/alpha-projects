package com.telerikacademy.cosmetics.models.common;

public enum UsageType {
    MEDICAL,
    EVERYDAY
}
