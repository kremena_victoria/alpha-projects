package com.telerikacademy.cosmetics.models.common;

public enum Scent {
    LAVENDER,
    VANILLA,
    ROSE
}
