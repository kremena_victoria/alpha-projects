package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Cream;
import com.telerikacademy.cosmetics.models.common.Scent;

public class CreamImpl implements Product, Cream {

    public static final String CREAM_INVALID_MSG = "Cream price must me positive number";
    public static final String CREAM_BRAND_INVALID_MSG = "Cream brand should ne between three and fifteen symbols";
    public static final String CREAM_NAME_INVALID_MSG = "Cream name should be between three and fifteen symbols.";
    public static final int CREAM_BRAND_MIN_LEN = 3;
    public static final int CREAM_BRAND_MAX_LEN = 15;
    public static final int CREAM_NAME_MIN_LEN = 3;
    public static final int CREAM_NAME_MAX_LEN = 15;
    private String creamName;
    private String creamBrand;
    private double creamPrice;
    private GenderType creamGender;
    private Scent creamScent;

    public CreamImpl(String v1, String v2, double v3, GenderType women, Scent rose) {
        setCreamName(v1);
        setCreamBrand(v2);
        setCreamPrice(v3);
        this.creamGender = women;
        this.creamScent = rose;
    }

    @Override
    public Scent getScent() {
        return creamScent;
    }

    @Override
    public String getName() {
        return creamName;
    }

    @Override
    public String getBrand() {
        return creamBrand;
    }

    @Override
    public double getPrice() {
        return creamPrice;
    }

    @Override
    public GenderType getGender() {
        return creamGender;
    }

    @Override
    public String print() {
        return String.format(" #%s %s\n" +
                " #Price: %f\n" +
                "#Gender: %s\n" +
                "#Scent: %s\n" +
                "===", creamName, creamBrand,creamPrice,creamGender,creamScent);
    }

    private void setCreamName(String creamName) {
        if(creamName.length() < CREAM_NAME_MIN_LEN || creamName.length() > CREAM_NAME_MAX_LEN){
            throw new IllegalArgumentException(CREAM_NAME_INVALID_MSG);
        }
        this.creamName = creamName;
    }


    private void setCreamBrand(String creamBrand) {
        if(creamBrand.length() < CREAM_BRAND_MIN_LEN || creamBrand.length() > CREAM_BRAND_MAX_LEN){
            throw new IllegalArgumentException(CREAM_BRAND_INVALID_MSG);
        }
        this.creamBrand = creamBrand;
    }

    private void setCreamPrice(double creamPrice) {
        if(creamPrice <= 0){
            throw new IllegalArgumentException(CREAM_INVALID_MSG);
        }
        this.creamPrice = creamPrice;
    }


}
