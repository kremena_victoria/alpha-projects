package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ShampooImpl implements Product, Shampoo {

    public static final int SHAMPOO_MIN_NAME_LEN = 3;
    public static final int SHAMPOO_MAX_NAME_LEN = 10;
    public static final int SHAMPOO_MIN_BRAND_LEN = 2;
    public static final int SHAMPOO_MAX_BRAND_LEN = 10;
    public static final String NAME_INVALID_MSG = "Shampoo name should be between three and ten symbols.";
    public static final String SHAMPOO_BRAND_INVALID_MSG = "Shampoo brand should ne between two and ten symbols";
    public static final String SHAMPOO_PRICE_INVALID_MSG = "Shampoo price must me not negative number";
    public static final String SHAMPOO_LITERS_INVALID_MSG = "Shampoo milliliters must be not negative number";

    private String shampooName;
    private String shampooBrand;
    private double shampooPrice;
    private GenderType shampooGender;
    private int shampooMilliliters;
    private UsageType shampooUsage;

    public ShampooImpl(String v1, String v2, double v3, GenderType women, int v4, UsageType medical) {
        setShampooName(v1);
        setShampooBrand(v2);
        setShampooPrice(v3);
        this.shampooGender = women;
        setShampooMilliliters(v4);
        this.shampooUsage = medical;
    }

    @Override
    public int getShampooMilliliters() {
        return shampooMilliliters;
    }

    @Override
    public UsageType getUsage() {
        return shampooUsage;
    }

    @Override
    public String getName() {
        return shampooName;
    }

    @Override
    public String getBrand() {
        return shampooBrand;
    }

    @Override
    public double getPrice() {
        return shampooPrice;
    }

    @Override
    public GenderType getGender() {
        return shampooGender;
    }

    @Override
    public String print() {
        return String.format(" #%s %s\n" +
                " #Price: %f\n" +
                "#Gender: %s\n" +
                "#Milliliters: %d\n" +
                "#Usage: %s\n" +
                "===", shampooName, shampooBrand,shampooPrice,shampooGender,shampooMilliliters,shampooUsage);
    }

    private void setShampooName(String shampooName) {
        if( shampooName == null || shampooName.length() < SHAMPOO_MIN_NAME_LEN || shampooName.length() > SHAMPOO_MAX_NAME_LEN){
            throw new IllegalArgumentException(NAME_INVALID_MSG);
        }
        this.shampooName = shampooName;
    }

    private void setShampooBrand(String shampooBrand) {
        if(shampooBrand == null || shampooBrand.length() < SHAMPOO_MIN_BRAND_LEN || shampooBrand.length() > SHAMPOO_MAX_BRAND_LEN){
            throw new IllegalArgumentException(SHAMPOO_BRAND_INVALID_MSG);
        }
        this.shampooBrand = shampooBrand;
    }

    private void setShampooPrice(double shampooPrice) {
        if(shampooPrice < 0){
            throw new IllegalArgumentException(SHAMPOO_PRICE_INVALID_MSG);
        }
        this.shampooPrice = shampooPrice;
    }

    private void setShampooMilliliters(int shampooMilliliters) {{
        if(shampooMilliliters < 0){
            throw new IllegalArgumentException(SHAMPOO_LITERS_INVALID_MSG);
        }
    }
        this.shampooMilliliters = shampooMilliliters;
    }

}
