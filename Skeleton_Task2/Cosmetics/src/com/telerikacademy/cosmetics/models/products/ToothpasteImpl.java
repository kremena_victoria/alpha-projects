package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl implements Product, Toothpaste {

    public static final int TOOTHPASTE_NAME_MIN_LEN = 3;
    public static final int TOOTHPASTE_NAME_MAX_LEN = 10;
    public static final String TOOTHPASTE_NAME_INVALID_MSG = "Toothpaste name should be between three and ten symbols.";
    public static final int TOOTHPASTE_BRAND_MIN_LEN = 2;
    public static final int TOOTHPASTE_BRAND_MAX_LEN = 10;
    public static final String TOOTHPASTE_BRAND_INVALID_MSG = "Toothpaste brand should ne between two and ten symbols";
    public static final String TOOTHPATE_PRICE_INVALID_MSG = "Toothpaste price must me not negative number";
    public static final String TOOTHAPASTE_INGREDIENTS_INVALID_MSG = "Ingredients cannot be empty string!";
    private String toothpasteName;
    private String toothpasteBrand;
    private double toothpastePrice;
    private GenderType toothpasteGender;
    private List<String> toothpasteIngredients;


    public ToothpasteImpl(String v1, String v2, double v3, GenderType women, List<String> v4) {
        setToothpasteName(v1);
        setToothpasteBrand(v2);
        setToothpastePrice(v3);
        this.toothpasteGender = women;
        setToothpasteIngredients(v4);

    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(toothpasteIngredients);
    }

    @Override
    public String getName() {
        return toothpasteName;
    }

    @Override
    public String getBrand() {
        return toothpasteBrand;
    }

    @Override
    public double getPrice() {
        return toothpastePrice;
    }

    @Override
    public GenderType getGender() {
        return toothpasteGender;
    }

    @Override
    public String print() {
        /*
        #Category: Toothpastes
        #White Colgate
        #Price: $10.99
        #Gender: Men
        #Ingredients: [calcium, fluorid]
        ===
         */
        return String.format(" #%s %s\n" +
                " #Price: %f\n" +
                "#Gender: %s\n" +
                "#Ingredients: %s\n" +
                "===", toothpasteName, toothpasteBrand,toothpastePrice,toothpasteGender,toothpasteIngredients);
    }

    public void setToothpasteName(String toothpasteName) {
        if(toothpasteName.length() < TOOTHPASTE_NAME_MIN_LEN || toothpasteName.length() > TOOTHPASTE_NAME_MAX_LEN){
            throw new IllegalArgumentException(TOOTHPASTE_NAME_INVALID_MSG);
        }
        this.toothpasteName = toothpasteName;
    }

    public void setToothpasteBrand(String toothpasteBrand) {
        if(toothpasteBrand.length() < TOOTHPASTE_BRAND_MIN_LEN || toothpasteBrand.length() > TOOTHPASTE_BRAND_MAX_LEN){
            throw new IllegalArgumentException(TOOTHPASTE_BRAND_INVALID_MSG);
        }
        this.toothpasteBrand = toothpasteBrand;
    }

    public void setToothpastePrice(double toothpastePrice) {
        if(toothpastePrice < 0){
            throw new IllegalArgumentException(TOOTHPATE_PRICE_INVALID_MSG);
        }
        this.toothpastePrice = toothpastePrice;
    }
    // da go napravia!!!
    public void setToothpasteIngredients(List<String> toothpasteIngredients) {
        if (toothpasteIngredients == null) {
            throw new IllegalArgumentException(TOOTHAPASTE_INGREDIENTS_INVALID_MSG);
        }
        this.toothpasteIngredients = toothpasteIngredients;
    }
}
