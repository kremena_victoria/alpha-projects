package com.telerikacademy.cosmetics.models.contracts;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.Scent;


public interface Cream extends Product{

    Scent getScent();
}
