package com.telerikacademy.cosmetics.commands;

import com.telerikacademy.cosmetics.core.contracts.Command;
import com.telerikacademy.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.cosmetics.models.common.Scent;
import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Cream;
//import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.cosmetics.commands.CommandConstants.CREAM_ALREADY_EXIST;
import static com.telerikacademy.cosmetics.commands.CommandConstants.CREAM_CREATED;

public class CreateCream implements Command {
    private CosmeticsRepository cosmeticsRepository;
    private CosmeticsFactory cosmeticsFactory;

    public CreateCream(CosmeticsRepository cosmeticsRepository, CosmeticsFactory cosmeticsFactory) {
        this.cosmeticsRepository = cosmeticsRepository;
        this.cosmeticsFactory = cosmeticsFactory;
    }

    @Override
    public String execute(List<String> parameters) {
        //throw new NotImplementedException();
        String creamName = parameters.get(0);
        String creamBrand = parameters.get(1);
        double creamPrice = Double.parseDouble(parameters.get(2));
        GenderType creamGender = GenderType.valueOf(parameters.get(3).toUpperCase());
        Scent creamScent = Scent.valueOf(parameters.get(4).toUpperCase());
        return createCream(creamName, creamBrand, creamPrice, creamGender, creamScent);
    }

    private String createCream(String creamName, String creamBrand, double creamPrice, GenderType creamGender, Scent creamScent) {
        if (cosmeticsRepository.getProducts().containsKey(creamName)) {
            return String.format(CREAM_ALREADY_EXIST, creamName);
        }

        Cream cream = cosmeticsFactory.createCream(creamName, creamBrand, creamPrice, creamGender, creamScent);
        cosmeticsRepository.getProducts().put(creamName, cream);

        return String.format(CREAM_CREATED, creamName);
    }
}
