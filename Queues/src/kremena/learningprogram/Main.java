package kremena.learningprogram;

public class Main {

    public static void main(String[] args) {
        Employee p = new Employee("Pesho", "Delchev", 234);
        Employee k = new Employee("Karolina", "Thomas", 333);
        Employee m = new Employee("Mary", "Jane", 33);

        ArrayQueue queue = new ArrayQueue(10);

        queue.add(p);
        queue.add(k);
        queue.add(m);
        System.out.println(queue.peek());
        System.out.println();

        queue.remove();
        queue.printQueue();
    }
}
