package kremena.learningprogram;

import java.util.LinkedList;
import java.util.ListIterator;

//implemets dequee => implements push, pop and peek

public class LinkedStack {

    //here the time complexity is only O(1)!!!

    private LinkedList<Employee> stack;

    public LinkedStack() {
        stack = new LinkedList<Employee>();
    }

    public void push(Employee employee){
        stack.push(employee);
    }

    public Employee pop(){
        return stack.pop();
    }

    public Employee peek(){
        return stack.peek();
    }

    public boolean isEmpty(){
        return stack.isEmpty();
    }

    public void printStack(){
        ListIterator<Employee> iterator = stack.listIterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }

    public int size(){
        return stack.size();
    }
}
