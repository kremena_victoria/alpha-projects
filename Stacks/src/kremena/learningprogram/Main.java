package kremena.learningprogram;

public class Main {

    public static void main(String[] args) {
        ArrayStack stack = new ArrayStack(10);

        stack.push(new Employee("Kremena", "Georgieva", 123));
        stack.push(new Employee("Joan", "Piontek", 117));
        stack.push(new Employee("Jane", "Johnes", 456));
        stack.push(new Employee("Gerd", "Groll", 678));
        stack.push(new Employee("Bill", "End", 22));

        //stack.printStack();

        //System.out.println(stack.peek());
        //stack.printStack();

        //System.out.println("Popped: "+stack.pop());
        //System.out.println(stack.peek());

        Employee p = new Employee("Pesho", "Delchev", 234);
        Employee k = new Employee("Karolina", "Thomas", 333);

        LinkedStack linkedStack = new LinkedStack();
        linkedStack.push(p);
        linkedStack.push(k);
        //linkedStack.printStack();
        System.out.println(linkedStack.peek());
        System.out.println("Popped: "+linkedStack.pop());
        linkedStack.printStack();
        System.out.println("The size is: "+linkedStack.size());

    }
}
